#pragma once
class MemoryException {
public:
  virtual void showError() = 0;
};
